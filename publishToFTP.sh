#!/bin/bash
#
# Parameters:
# $1 = FTP Root directory on the FTP site. If Jenkins "powerpmac/firmware/jenkins/"
# $2 = FTP source directory within ppmacx repository. Usually $(STAGE_DIR)
# $3 = FTP PLATFORM directory. A sub directory of FTP root. Usually "PPMAC-$(PLATFORM)-$(KERNEL_VERSION)"
# $4 = FTP build directory. Usually $(BUILD_VERSION) from the makeVersionFile.sh script program.
# $5 = File server where binary files (artifacts) are to be published for storage and dissemination.
# $6 = Verbose, off by default, -v turns verbose output messaging ON.

# Create the version.txt file from version.h
bv=$(./makeVersionFile.sh)

# Set the default values
ftp_rootdir='powerpmac/firmware/jenkins/'
ftp_sourceDir=$STAGE_DIR
ftp_platformdir=PPMAC-${PLATFORM}-${KERNEL_VERSION}
file_server=ftp
ftp_site=fwupdate.deltatau.com
ftp_builddir=${bv}
vb=
# TODO - Change (check out) to brance requested???
cb=`git branch | awk -v FS=' ' '/\*/{print $NF}' | sed 's|[()]||g'`

usage()
{
    echo "usage: publishToFtp   [[[-option | --option dir ] [-v]] | [-h]]"
    echo "usage: publishToSftp  [[[-option | --option dir ] [-v]] | [-h]]"
    echo "usage: publishToTbd   [[[-option | --option dir ] [-v]] | [-h]]"
    echo "  -r | --rootdir      FTP Root directory on the FTP site. If Jenkins. Default: powerpmac/firmware/jenkins/"
    echo "  -s | --sourcedir    FTP source directory within ppmacx repository. Default: ppmacx/stage/PLATFORM-KERNEL_VERSION"
    echo "  -p | --platformdir  FTP PLATFORM directory. A sub directory of FTP root. Default: PPMAC-PLATFORM-KERNEL_VERSION"
    echo "  -b | --builddir     FTP build directory. Default: BUILD_VERSION from the makeVersionFile.sh script program."
    echo "  -f | --fileserver   Hard coded storage location for debian pkg's, e.g. publishToFtp = -f ftp | publishToSftp = -f sftp | publishToTbd = -f tbd."
    # echo "  -c | --gitbranch    GIT branch. Default: command: git branch."
    echo "  -v | --verbose      FTP Verbose mode."
    echo "  -h | --help         This usage/help."
}

while [ "$1" != "" ]; do
    case $1 in
        -r | --rootdir )        shift
                                ftp_rootdir=$1
                                ;;
        -s | --sourcedir )      shift
                                ftp_sourceDir=$1
                                ;;
        -p | --platformdir )    shift
                                ftp_platformdir=$1
                                ;;
        -b | --builddir )       shift
                                ftp_buildDir=$1
                                ;;
        -f | --fileserver )     shift
                                file_server=$1
                                ;;
        -c | --commit )         shift
                                cb=$1
                                ;;
        -v | --verbose )        vb=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

# Flatten branch name by changing '/' to '-'. (i.e. feature/myfeature)
commit_branch=`echo $cb | tr / -`

commit_year=`git log -1 --format="%at" | xargs -I{} date -d @{} +%Y`
commit_month=`git log -1 --format="%at" | xargs -I{} date -d @{} +%m`
commit_day=`git log -1 --format="%at" | xargs -I{} date -d @{} +%dT%H_%M_%S`

if [ "$vb" = "1" ]; then
    echo '* ftp_rootdir     = ' ${ftp_rootdir}
    echo '* ftp_sourceDir   = ' ${ftp_sourceDir}
    echo '* ftp_platformdir = ' ${ftp_platformdir}
    echo '* ftp_builddir    = ' ${ftp_builddir}
    echo '* file_server     = ' ${file_server}
    echo '* ftp_site        = ' ${ftp_site}
    echo '* commit_branch   = ' ${commit_branch}
    echo '* commit_year     = ' ${commit_year}
    echo '* commit_month    = ' ${commit_month}
    echo '* commit_day      = ' ${commit_day}
    ftpopen="-inv"
    ftp_options="-v"
else
    ftpopen="-in"
    ftp_options=
fi

sendFileByFtp()
{
  ftp_site=fwupdate.deltatau.com
  ftp_username=fwrdwrt
  ftp_passwd='fw$rd$wrt!123'

  cd $ftp_sourceDir

ftp $ftpopen <<EOF
open $ftp_site
user $ftp_username $ftp_passwd
bin
cd $ftp_rootdir
mkdir $ftp_platformdir
cd $ftp_platformdir
mkdir $commit_year
cd $commit_year
mkdir $commit_month
cd $commit_month
mkdir $commit_branch
cd $commit_branch
mkdir $commit_day-$ftp_builddir
cd $commit_day-$ftp_builddir
mput *.deb
mput *.gz
mput *.zip
mput *.log
mput *.txt
close
bye
EOF
}

sendFileBySftp()
{
  ftp_site=10.150.168.195
  ftp_port=22
  ftp_username=fwrdwrt

  cd $ftp_sourceDir

sftp $ftp_options -oPasswordAuthentication=no -oRSAAuthentication=yes -oPort=$ftp_port $ftp_username@$ftp_site <<EOF
cd $ftp_rootdir
mkdir $ftp_platformdir
cd $ftp_platformdir
mkdir $commit_year
cd $commit_year
mkdir $commit_month
cd $commit_month
mkdir $commit_branch
cd $commit_branch
mkdir $commit_day-$ftp_builddir
cd $commit_day-$ftp_builddir
mput *.deb
mput *.gz
mput *.zip
mput *.log
mput *.txt
bye
EOF
}

sendFileByTbd()
{
    # stub for new file server, or cloud
    ftp_site="tbd"
    echo "future location of published artifacts"
}

# publish files to selected server
shopt -s nocasematch
case $file_server in
    ftp )
        # xfer files to original ftp server
        sendFileByFtp
        echo "* Transfered files to: ${file_server} ${ftp_site} "
        ;;
    sftp )
        # xfer files to sftp server
        sendFileBySftp
        echo "* Transfered files to: ${file_server} ${ftp_site} "
        ;;
    tbd )
        # xfer files to tbd server
        sendFileByTbd
        echo "* Transfered files to: ${file_server} ${ftp_site} "
        ;;
    * )
        # Default: xfer files to sftp server
        sendFileBySftp
        echo "* Default - Transfered files to: ${file_server} ${ftp_site} "
        ;;
esac
shopt -u nocasematch

exit 0
