#!/bin/bash

vers_string=`grep  FULLVERSION_STRING ./version.h`
temp_vers_string=${vers_string##*=}
vername=`echo "$temp_vers_string" | sed 's/"//g' | sed 's/ //g' | sed 's/;//'`

incVersion=$(echo $vername| awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{$NF=sprintf("%0*d", length($NF), ($NF+1)); print}')
shortCommit=`git log -n 1 --pretty=format:'%h'`

# branch=`git rev-parse --abbrev-ref HEAD`
branch=`git branch | awk -v FS=' ' '/\*/{print $NF}' | sed 's|[()]||g'`
# branch=`git symbolic-ref HEAD | sed -e "s/^refs\/heads\///"`
# currentTime=`date -Iseconds`
`git log -n 1 > commit.log`
`git diff-tree --patch-with-stat -r HEAD > change.log`

versionCommit="v$vername-$shortCommit"
branchVersionCommit="$branch/$versionCommit"
echo $branchVersionCommit>branchVersionCommit.txt
echo $branchVersionCommit
