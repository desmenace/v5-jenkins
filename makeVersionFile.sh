#!/bin/bash

vers_string=`grep  FULLVERSION_STRING ./version.h`
temp_vers_string=${vers_string##*=}
vername=`echo "$temp_vers_string" | sed 's/"//g' | sed 's/ //g' | sed 's/;//'`

incVersion=$(echo $vername| awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{$NF=sprintf("%0*d", length($NF), ($NF+1)); print}')
shortCommit=`git log -n 1 --pretty=format:'%h'`
commitTag=`git tag --points-at HEAD`
branch=`git branch | awk -v FS=' ' '/\*/{print $NF}' | sed 's|[()]||g'`

if [ "$commitTag" != "" ]; then
    versionCommit="$commitTag-$shortCommit"
else
    versionCommit="v$vername-$shortCommit"
fi
echo $versionCommit>version.txt
echo $versionCommit