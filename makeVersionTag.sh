#!/bin/bash

vers_string=`grep  FULLVERSION_STRING ./version.h`
temp_vers_string=${vers_string##*=}
vername=`echo "$temp_vers_string" | sed 's/"//g' | sed 's/ //g' | sed 's/;//'`

incVersion=$(echo $vername| awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{$NF=sprintf("%0*d", length($NF), ($NF+1)); print}')

shortCommit=`git log -n 1 --pretty=format:'%h'`
commitTime=`git log -1 --format="%at" | xargs -I{} date -d @{} +%Y/%m/%dT%H_%M_%S`
commitTag=`git tag --points-at HEAD`
commitBranch=`git branch | awk -v FS=' ' '/\*/{print $NF}' | sed 's|[()]||g'`

# Write git commit information files
git log -n 1 > commit.log
git diff-tree --patch-with-stat -r HEAD > change.log

versionCommit="v$vername-$shortCommit"
versionCommitTag="$commitTime-$versionCommit"
echo $versionCommitTag>versionCommitTag.txt
# echo $versionCommitTag

EOL=$'\n\r'
DBQ=$'\"'
# TAB=$'\t'

newVersionHeader=$"#ifndef VERSIONCOMMIT_H$EOL#define VERSIONCOMMIT_H$EOL\
    $TABstatic const char FULLVERSION_STRING[] = $DBQ$vername$DBQ;$EOL\
    $TABstatic const char VERSION_STRING[] = $DBQ$versionCommit$DBQ;$EOL\
    $TABstatic const char VERSION_COMMIT[] = $DBQ$shortCommit$DBQ;$EOL\
    $TABstatic const char VERSION_TAG[] = $DBQ$commitTag$DBQ;$EOL\
    $TABstatic const char VERSION_COMMIT_TAG[] = $DBQ$versionCommitTag$DBQ;$EOL\
    #endif //VERSIONCOMMIT_h$EOL"
echo $newVersionHeader>versionCommit.h

#versioncmake=$"# PPMACX Versions$EOL \
#function(set_ppmacx_version)$EOL \
#  set(PPMACX_VERSION $DBQ$vername$DBQ)$EOL \
#  set(PPMACX_VERSION_STRING $DBQ$versionCommit$DBQ)$EOL \
#  set(PPMACX_VERSION_COMMIT $DBQ$shortCommit$DBQ)$EOL \
#  set(PPMACX_VERSION_TAG $DBQ$commitTag$DBQ)$EOL \
#endfunction()$EOL"
#echo $versioncmake>version.cmake

export FULLVERSION_STRING=$vername
export VERSION_STRING=$versionCommit
export VERSION_COMMIT=$shortCommit
export VERSION_TAG=$commitTag
export VERSION_COMMIT_TAG=$versionCommitTag

echo "$vername"


