#!/bin/bash

CWD=$(pwd)
# set defaults
PROJDIR="${CWD}"
LAYERSDIR="${PROJDIR}/layers"
BRANCH="hardknott"

usage()
{
    echo -e "\nUsage: ocr-setup-develop.sh
    Optional parameters: [-n name] [-b branch] [-d build-dir] [-h]"
echo "
    * [-b branch]:    Yocto repositories branch name
    * [-d build-dir]: Build directory, if unspecified script uses '$HOME/workspace' as output directory
    * [-h]: help - this message
"
}

clone-layer()
{
    echo "Layer: $1"
    cd "$LAYERSDIR" || clean_up 2
    git clone -b "$2" "$3"
    cd "$CWD" ||  clean_up 2
    echo ""
}

clone-project()
{
    echo "Project: $1"
    cd "$PROJDIR" ||  clean_up 2
    git clone -b "$2" "$3"
    cd "$CWD" ||  clean_up 2
    echo ""
}

clean_up()
{
    unset CWD BUILD_DIR
    unset LayerArray ProjectArray
    unset setup_help setup_error setup_flag
    case $1 in
        0) echo "Success";;
        1) echo "Error";;
        2) echo "CD to unknown directory";;
        *) echo "Unknown Error";;
    esac
    exit "$1"
}

while getopts b:d:h: opt; do
    case $opt in
        b) BRANCH=$OPTARG;;
        d) PROJDIR=$OPTARG;LAYERSDIR="${PROJDIR}/layers";;
        h) setup_help="true";;
        *) echo "WARN: '-${OPTARG}' isn't a valid option; I'm ignoring it.";;
    esac
done
shift "$OPTIND - 1"

# Layers required -------------------------------------------------------------
declare -a LayerArray
# POKY
LayerArray[0]="poky;$BRANCH;git://git.yoctoproject.org/poky"
# META_BROWSER no HARDKNOTT
LayerArray[1]="meta-browser;master;https://github.com/OSSystems/meta-browser.git"
# META_CLANG
LayerArray[2]="meta-clang;$BRANCH;https://github.com/kraj/meta-clang"
# META_OPENEMBEDDED
LayerArray[3]="meta-openembedded;$BRANCH;git://git.openembedded.org/meta-openembedded"
# META_QT5
LayerArray[4]="meta-qt5;$BRANCH;https://github.com/meta-qt5/meta-qt5.git"
# META_PYTHON2
LayerArray[5]="meta-python2;$BRANCH;git://git.openembedded.org/meta-python2"
# META_TIMESYS
LayerArray[6]="meta-timesys;$BRANCH;https://github.com/TimesysGit/meta-timesys.git"
# META_SECURITY
LayerArray[7]="meta-security;$BRANCH;git://git.yoctoproject.org/meta-security"
# META_FREESCALE
LayerArray[8]="meta-freescale;$BRANCH;git://git.yoctoproject.org/meta-freescale"
# META_FREESCALE_3RDPARTY
LayerArray[9]="meta-freescale-3rdparty;$BRANCH;https://github.com/Freescale/meta-freescale-3rdparty.git"
# META_FREESCALE_DISTRO
LayerArray[10]="meta-freescale-distro;$BRANCH;https://github.com/Freescale/meta-freescale-distro"
# META_IMX
LayerArray[11]="meta-imx;${BRANCH}-5.10.52-2.1.0;https://source.codeaurora.org/external/imx/meta-imx"
# META-READONLY-ROOTFS-OVERLAY
LayerArray[12]="meta-readonly-rootfs-overlay;master;https://github.com/cmhe/meta-readonly-rootfs-overlay"
# META_IMX_OCR
LayerArray[13]="meta-imx-ocr;$BRANCH;ssh://git@oatbitbucket.am.omron.net:7999/ocrv5/meta-imx-ocr.git"

# Projects available ----------------------------------------------------------
declare -a ProjectArray
# V5_DEV
ProjectArray[0]="v5-imx8mpevk;develop;ssh://git@oatbitbucket.am.omron.net:7999/ocrv5/v5-imx8mpevk.git"
# V5
ProjectArray[1]="v5-imx8mp-ocr;develop;ssh://git@oatbitbucket.am.omron.net:7999/ocrv5/v5-imx8mp-ocr.git"


# OPTIND=$OLD_OPTIND
if test $setup_help; then
    usage && clean_up 3
fi

# getPassword

# Create the tree
if [ -d "$LAYERSDIR" ]; then
    echo "$LAYERSDIR is available"
else
    echo "Create $LAYERSDIR"
    install -d "$LAYERSDIR"
fi

# Layers
for layer in "${LayerArray[@]}"
do
    IFS=";" read -r -a arr <<< "${layer}"
    clone-layer "${arr[0]}" "${arr[1]}" "${arr[2]}";
done

#projects
for project in "${ProjectArray[@]}"
do
    IFS=";" read -r -a arr1 <<< "${project}"
    clone-project "${arr1[0]}" "${arr1[1]}" "${arr1[2]}";
done

clean_up 0
